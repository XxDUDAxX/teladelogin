package com.ghqsouza.telalogin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText entradaNome, entradaSenha;
    private Button botaoEntrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicializarComponentes ();

        //AÇÃO DO BOTÃO
        botaoEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               String nome = entradaNome.getText().toString();
               String senha = entradaSenha.getText().toString();

               // EXIBIR O NOME E SENHA COM "TOAST"


                Toast.makeText(getApplicationContext(),"Olá" + nome +
                        "Você digitou a senha " + senha, Toast.LENGTH_LONG).show();



            }
        });
    }

    private void inicializarComponentes() {
        entradaNome = findViewById(R.id.editNomeID);
        entradaSenha = findViewById(R.id.editSenhaID);
        botaoEntrar = findViewById(R.id.btEntrarID);

    }
}
